# AURA Play

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](code_of_conduct.md) [![Latest Release](https://gitlab.servus.at/aura/play/-/badges/release.svg)](https://gitlab.servus.at/aura/play/-/releases) [![pipeline status](https://gitlab.servus.at/aura/play/badges/main/pipeline.svg)](https://gitlab.servus.at/aura/play/-/commits/main)

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-play-circle.png" width="250" align="right" />

**AURA Play is a framework agnostic user interface toolbox** for integrating the AURA Radio Software Suite into your website and Content Management System.

These JavaScript components are written in [Svelte](https://svelte.dev/docs) and are reading data from the [AURA API](https://api.aura.radio). As a result all components are provided in a lightweight package. They are pluggable into any front-end framework or website.

Some of the components are provided as true [HTML 5 Web Components](https://en.wikipedia.org/wiki/Web_Components), most others are delivered as [Svelte Components](https://svelte.dev/). The latter provide easier integration and styling options and are therefore preferred for future development.

Component features:

- [Cards Design Pattern](https://www.justinmind.com/blog/cards-ui-design/)
- Based on [Material Design](https://material.io/), more specifically [SMUI](https://sveltematerialui.com/)
- [Responsive Design](https://en.wikipedia.org/wiki/Responsive_web_design), supporting a wide variety of mobile and desktop devices
- Solid foundation for [accessibility](https://material.io/design/usability/accessibility.html#understanding-accessibility) requirements
- Themeable and stylable, including dark themes.
- [TypeScript](https://www.typescriptlang.org/) support
- [SCSS](https://sass-lang.com/) support

Provided components:

- (Auto) Radio Player - A minimalist radio player (Important: Currently only integrated with the o94 legacy player popup)
- Programme Information - A Web Component to render radio programme infos in various forms (Currently only integrated with o94)
- Track Service (_WIP_: not up-to-date with latest `engine-api`)
- Service Worker - A HTML 5 `service-worker.js` including a Web Component to load the Service Worker plus sample `pre-cache.json` and `manifest.json` files
- Online Status - A _Toast Notification_ component for online/offline status changes
- Host List Component
- Host Detail Component
- Show List Component
- Show Detail Component

> **Disclaimer:** The components of this repository are _Work in Progress_ and hence not fully featured nor documented in all aspects. Currently some parts are customized for an Radio Orange 94.0 integration. This component will be generalized in the future upon request from third-parties. In addition Radio ORANGE 94.0 developed a Drupal 9 integration based on the AURA Play components. If you are interested using AURA in conjunction with Drupal, get in touch. Also, these components are using a simulated AURA API for now. This API is hosted at https://prog-info.o94.at/

<!-- TOC -->

1. [AURA Play](#aura-play)
   1. [Website Integration](#website-integration)
   2. [Components](#components)
      1. [Media Player](#media-player)
      2. [Media Player Button](#media-player-button)
      3. [Now Playing Widget](#now-playing-widget)
      4. [Programme Information](#programme-information)
      5. [Track Service](#track-service)
      6. [Online Status (Toast Notification)](#online-status-toast-notification)
      7. [Host](#host)
         1. [Host List Component](#host-list-component)
         2. [Host Detail Component](#host-detail-component)
      8. [Show](#show)
         1. [Show List Component](#show-list-component)
         2. [Show Detail Component](#show-detail-component)
      9. [Episode](#episode)
         1. [Episode Detail Component](#episode-detail-component)
      10. [Category](#category)
          1. [Category List Component](#category-list-component)
   3. [Progressive Web App](#progressive-web-app)
      1. [Service Worker](#service-worker)
      2. [Manifest](#manifest)
      3. [Offline mode](#offline-mode)
         1. [Offline mode with cached pages](#offline-mode-with-cached-pages)
         2. [Offline mode with `offline.html`](#offline-mode-with-offlinehtml)
   4. [Development](#development)
      1. [Requirements](#requirements)
      2. [Install](#install)
      3. [Run](#run)
   5. [Production](#production)
2. [License](#license)
3. [Read more](#read-more)

<!-- /TOC -->

## Website Integration

## Components

### Media Player

Renders the Radio Media Player.

```html
<div id="media-player"></div>
<script>
  /** Inject component into HTML **/
  document.addEventListener('DOMContentLoaded', function (event) {
    new AuraMediaPlayer({
      target: document.getElementById('media-player'),
      props: {
        renderHtml: true,
        stream: 'https://securestream.o94.at/live.mp3',
      },
    })
  })
</script>
```

### Media Player Button

Display a button to either add a track to playlist (`type='add'`) or add and play (`type='play'`).

This currently only works when the player is part of the page, not when being used in popup mode.

```html
<div class="demo-album component-container">
  <div class="album-art lorn"></div>
  <div
    id="media-player-button-play"
    class="media-player-button component-container"
  ></div>
</div>
<script>
  /** Inject component into HTML **/
  document.addEventListener('DOMContentLoaded', function (event) {
    new AuraMediaPlayerButton({
      target: document.getElementById('media-player-button-play'),
      props: {
        type: 'play',
        label: 'Play',
        track: {
          name: 'Anvil',
          artist: 'Lorn',
          album: 'Anvil',
          url: '/path/to/audio.mp3',
          cover: '/path/to/audio-cover.png',
        },
      },
    })
  })
</script>
```

### Now Playing Widget

Display a bar with the current show and episode details. When clicking the bar the media player opens in a popup window.

```html
<div id="media-player-widget" class=""></div>
<script>
  /** Inject component into HTML **/
  document.addEventListener('DOMContentLoaded', function (event) {
    new AuraNowPlayingWidget({
      target: document.getElementById('media-player-widget'),
      props: { popupUrl: '/player_popup.html' },
    })
  })
</script>
```

### Programme Information

Renders the daily programme info by listing all shows of the selected day.

```html
<div id="aura-programme"></div>
<script>
  /** Inject component into HTML **/
  document.addEventListener('DOMContentLoaded', function (event) {
    new AuraProgramme({
      target: document.getElementById('aura-programme'),
      props: {
        date: 'today',
        limitCount: 0,
        urlEpisodeDetail: '/episode-detail.html?id=',
        renderHtml: true,
      },
    })
  })
</script>
```

### Track Service

Track Service is a Web Component which displays the track currently played by the radio station. Additionally there's a history of past and future tracks.

Render the component in the desired place of your website like this:

```html
<div id="aura-trackservice"></div>
<script>
  /** Inject component into HTML **/
  document.addEventListener('DOMContentLoaded', function (event) {
    new AuraTrackService({
      target: document.getElementById('aura-trackservice'),
      props: {
        labelTitle: 'Which track is playing right now?',
      },
    })
  })
</script>
```

### Online Status (Toast Notification)

Displays a toast notification whether the app moved to an _online_ or _offline_ state.

```html
<div id="online-status-container"></div>
<script>
  /** Inject component into HTML **/
  document.addEventListener('DOMContentLoaded', function (event) {
    new OnlineStatus({
      target: document.getElementById('online-status-container'),
      props: {
        onlinetext: 'You are back online.',
        offlinetext: 'You have no connection.',
      },
    })
  })
</script>
```

### Host

#### Host List Component

Displays a list of _Host Cards_, including pagination.

```html
<div id="host-list"></div>
<script>
  /** Inject component into HTML **/
  document.addEventListener('DOMContentLoaded', function (event) {
    new AuraHostList({
      target: document.getElementById('host-list'),
      props: {
        renderHtml: true,
        urlHostDetail: '/host-detail.html?id=',
        activeonly: true,
        page: 0,
        limit: 600,
      },
    })
  })
</script>
```

#### Host Detail Component

Displays a _Host Detail Card_.

```html
<div id="host-detail"></div>
<script>
  /** Inject component into HTML **/
  document.addEventListener('DOMContentLoaded', function (event) {
    new AuraHostDetail({
      target: document.getElementById('host-detail'),
      props: {
        renderHtml: true,
        hostid: 707050,
        urlShowDetail: '/show-detail?slug=',
      },
    })
  })
</script>
```

### Show

#### Show List Component

Displays a list of _Show Card_.

```html
<div id="show-list"></div>
<script>
  /** Inject component into HTML **/
  document.addEventListener('DOMContentLoaded', function (event) {
    new AuraShowList({
      target: document.getElementById('show-list'),
      props: {
        renderHtml: true,
        urlShowDetail: '/show-detail.html?slug=',
        activeonly: true,
        limit: 600,
      },
    })
  })
</script>
```

#### Show Detail Component

Displays a _Show Detail Card_.

```html
<div id="show-detail"></div>
<script>
  /** Inject component into HTML **/
  document.addEventListener('DOMContentLoaded', function (event) {
    new AuraShowDetail({
      target: document.getElementById('show-detail'),
      props: {
        renderHtml: true,
        showSlug: 'female-pressure',
        urlEpisodeDetail: '/episode-detail.html?id=',
        urlHostDetail: '/host-detail.html?id=',
      },
    })
  })
</script>
```

### Episode

#### Episode Detail Component

Displays an _Episode Detail Card_.

```html
<div id="episode-detail"></div>
<script>
  /** Inject component into HTML **/
  document.addEventListener('DOMContentLoaded', function (event) {
    new AuraEpisodeDetail({
      target: document.getElementById('episode-detail'),
      props: {
        renderHtml: true,
        episodeId: randomId,
        labelPlay: '▶ Play',
        labelLinkArchive: 'Open in archive',
        urlShowDetail: '/show-detail.html?slug=',
        playerDomId: 'aura-player',
      },
    })
  })
</script>
```

### Category

#### Category List Component

Displays an overview of all or a defined list of categories.

```html
<div id="category-list"></div>
<script>
  /** Inject component into HTML **/
  document.addEventListener('DOMContentLoaded', function (event) {
    new AuraCategoryList({
      target: document.getElementById('category-list'),
      props: {
        renderHtml: true,
        urlShowList: '/show-list.html',
        categoriesToDisplay: [
          '1385089' /* Unter vielen */,
          '1385076' /* Im Unterschied */,
          '2060582' /* Auf Entdeckung */,
          '1385111' /* Spielarten */,
          '1385106' /* Der gute Ton */,
          '1385094' /* Im Interesse */,
          '1385100' /* Durchs Dickicht */,
        ],
      },
    })
  })
</script>
```

## Progressive Web App

The _AURA Play_ bundle also provides some building blocks to turn your website into a Progressive Web App. Keep in mind, that those are only serving as an entry point. For a fully-fledged PWA you'll need to add your own code and thoughts.

This repository also provides a sample app, allowing you to test the PWA beforehand. To start the app using the development server type:

```bash
./run.sh
```

### Service Worker

A production-ready Service Worker with offline functionality can be found under `public/service-worker.js`. Copy the Service worker to your web server and modify at least following variables:

```javascript
let OFFLINE_URL = '/offline.html' // Match your offline page URL
let PRE_CACHE = 'pre-cache-v1' // Manually update version or generate one server-site
let PRE_CACHE_URL = '/pre-cache.json' // Match your pre-cache URL
```

To load and install the Service Worker place the following custom tag somewhere on your page:

```html
<aura-service-worker></aura-service-worker>
```

The default component expects the service-service to be served via the URL fragment `/service-worker.js` and a JSON file holding resources to be pre-cached under `/pre-cache.json`. The pre-cached offline page should be served at `/offline.html`.

If you have custom requirements you can overwrite this behaviour by using the following attributes:

```html
<aura-service-worker url="/custom/path/service-worker.js"></aura-service-worker>
```

The Service Worker is based on a network-first strategy:

1. Incoming requests are first passed to the network
2. If the network is not available it queries the local cache
3. If the resource cannot be found in the cache, the pre-cached `offline.html` is returned

### Manifest

A sample Web Manifest is located in `public/manifest.json`. Copy this file to your web server and modify it according to your needs.

### Offline mode

The app provides multiple fallback stages when offline.

#### Offline mode with cached pages

Pages defined in `pre-cache.json` will be pre-cached upon Service Worker installation and hence are available offline. Copy this file to your web server and modify it according to your needs.

Perform following steps to verify the pre-cache to be working:

1. Start the development server and visit the start page. Do not visit the online page yet.
2. Stop the development server, but do not close the start page.
3. Refresh the start page. It will load as if a connection is available.

#### Offline mode with `offline.html`

To test the pages where the offline page is served as a fallback, do following:

1. Start the development server and visit the start page. Do not visit the `/online.html` page yet, to avoid being cached in the runtime cache.
2. Now the Service Worker is installed, pages in `public/pre-cache.json` plus `public/offline.html` are pre-cached.
3. Stop the development server, but do not close the start page.
4. Visit the URL `/online.html` via the link on the start page. Since this document is not stored in the cache, the default `/offline.html` will be returned.

If these steps do not work as described you'll need to ensure that either your cached is cleared beforehand, or you do increment the cache version in the `pre-cache.json` file.

## Development

### Requirements

- [Node.js 16](https://nodejs.org/) or later

### Install

```bash
make init.dev
```

Now create a file `config/settings.json` holding your configuration.

You can use the provided `sample-settings.json` as a basis. Update the relevant properties.

### Run

To start the development server execute

```bash
make run
```

## Production

Before building the production bundle, initialize the app environment.

```bash
make init.app
```

Then edit the `settings.json` located in the `config` folder.

To build the components for production run

```bash
make dist
```

The resulting build artifacts are located in `dist`.

Now include the bundle in your theme, template, dependency management system or however your website is organized:

1. Copy the artifacts `aura-play-bundle.js` and `aura-play-bundle.css` to your website assets. For pre-defined Material Design styles also grab `smui.css` and `smui-dark.css`.
2. Instantiate required components, as described in the [Components](#Components) section above.
3. Style the components using CSS. You can use the provided demo `public/aura-play-theme.css` as a starter.

In the future we may provide explicit support NPM installation and for SvelteKit integration.

# License

- Source code is licensed under [GNU AFFERO GENERAL PUBLIC LICENSE (AGPL v3)](LICENSE).
- The project icons and logos are by Martin Lasinger and licensed under [Creative Commons BY-NC-SA v3.0](https://creativecommons.org/licenses/by-nc-sa/3.0/).
- Component icons are from [Material Design Icons](https://materialdesignicons.com/) and licensed under [Pictogrammers Free License](https://github.com/Templarian/MaterialDesign/blob/master/LICENSE).

# Read more

- [docs.aura.radio](https://docs.aura.radio)
- [aura.radio](https://aura.radio)
