-include build/base.Makefile
-include build/docker.Makefile

help::
	@echo "$(APP_NAME) targets:"
	@echo "    init.app        - init application environment"
	@echo "    init.dev        - init development environment"
	@echo "    lint            - verify code style"
	@echo "    spell           - check spelling of text"
	@echo "    clean           - clean artifacts"
	@echo "    build           - build a production bundle"
	@echo "    dist            - create a dist package"
	@echo "    run             - start app in development mode"
	@echo "    start           - start app with sirv server (public)"
	$(call docker_help)


# Settings

TIMEZONE := "Europe/Vienna"

DOCKER_RUN = @docker run \
		--name $(APP_NAME) \
		--network="host" \
		-e TZ=$(TIMEZONE) \
		-u $(UID):$(GID) \
		$(DOCKER_ENTRY_POINT) \
		autoradio/$(APP_NAME)

# Targets

init.app:: package.json
	cp config/sample-settings.json config/settings.json
	npm install

init.dev::
	cp config/dev-settings.json config/settings.json
	npm install
	npm install -g cspell@latest

lint::
	npx svelte-check

spell::
	npm run spell

clean::
	rm -rf dist
	mkdir -p dist/assets

build:: TARGET:=dev
build::
	cp config/settings.json config/.backup.settings.json
	cp config/$(TARGET).settings.json config/settings.json
	npm run build
	cp config/.backup.settings.json config/settings.json

dist::clean
dist::build
dist::
	cp public/service-worker.js dist
	cp public/manifest.json dist
	cp public/build/smui-dark.css dist
	cp public/build/smui.css dist
	cp public/build/aura-play-bundle.* dist
	cp public/assets/*.svg dist/assets
	rm dist/assets/aura_fav.svg

run::
	npm run dev

start::
	npm run start --host