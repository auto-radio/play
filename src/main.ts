
import "./common.scss";

export * from 'amplitudejs';
/* Web Components */
export * from './components/ServiceWorker.wc.svelte';


/* Svelte Components */
import CategoryList from './components/CategoryList.svelte';
import EpisodeDetail from './components/EpisodeDetail.svelte';
import HostDetail from './components/HostDetail.svelte';
import HostList from './components/HostList.svelte';
import MediaPlayer from './components/MediaPlayer.svelte';
import MediaPlayerButton from './components/MediaPlayerButton.svelte';
import NowPlayingWidget from './components/NowPlayingWidget.svelte';
import OnlineStatus from './components/OnlineStatus.svelte';
import Player from './components/Player.svelte';
import Programme from './components/Programme.svelte';
import ShowDetail from './components/ShowDetail.svelte';
import ShowList from './components/ShowList.svelte';
import TrackService from './components/TrackService.svelte';

/** Inject non web-components into HTML **/
declare const window: any;


window.OnlineStatus = function (options : any) {
    return new OnlineStatus(options);
};
window.AuraNowPlayingWidget = function (options : any) {
    return new NowPlayingWidget(options);
};
window.AuraPlayer = function (options : any) {
    return new Player(options);
};
window.AuraMediaPlayer = function (options : any) {
    return new MediaPlayer(options);
};
window.AuraMediaPlayerButton = function (options : any) {
    return new MediaPlayerButton(options);
};
window.AuraHostList = function (options : any) {
    return new HostList(options);
};
window.AuraHostDetail = function (options : any) {
    return new HostDetail(options);
};
window.AuraShowList = function (options : any) {
    return new ShowList(options);
};
window.AuraShowDetail = function (options : any) {
    return new ShowDetail(options);
};
window.AuraEpisodeDetail = function (options : any) {
    return new EpisodeDetail(options);
};
window.AuraProgramme = function (options : any) {
    return new Programme(options);
};
window.AuraTrackService = function (options : any) {
    return new TrackService(options);
};
window.AuraCategoryList = function (options : any) {
    return new CategoryList(options);
};