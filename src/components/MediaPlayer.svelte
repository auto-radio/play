<script lang="ts">
  /**
   * @file Media Player Component.
   *
   *  The media player is organized in three tabs:
   *    - Currently playing: giving info on the current playlist item, including cover art.
   *    - Programme info: An except of the current radio station programme.
   *    - Playlist: holding live stream and audio file sources.
   *
   *  The media player allows playing audio sources from live stream (default) or references
   *  to audio files provided via URLs. Such audio file sources can be pushed to the playlist
   *  by populating the `addTrackStore` or passing the track information via URL parameters.
   *
   *  The url parameter `track` expects track metadata encoded with `common.Common.encodeUTF8`.
   *  The url parameter `play` (boolean) instructs the player to only queue  (`false`) or play
   *  the source immediately (`true`).
   *
   * @author David Trattnig <david@subsquare.at>
   */
  import { onMount, onDestroy, setContext } from 'svelte'
  import { addTrackStore } from '../stores.js'
  import { currentEpisodeStore } from '../poll.js'
  import { decodeUTF8 } from '../common/Common.svelte'
  import Spinner from '../common/Spinner.svelte'
  import MediaPlayerCenter from '../elements/MediaPlayerCenter.svelte'
  import Amplitude from 'amplitudejs'
  import Tab, { Icon } from '@smui/tab'
  import TabBar from '@smui/tab-bar'
  import Programme from './Programme.svelte'

  export var debug: boolean = true
  export var renderProgrammeLinks: boolean = true
  export var renderHtml: boolean = true
  export var autoPlay: boolean = true
  export var streamUrl: string = 'https://securestream.o94.at/live.mp3'
  export var streamName: string = 'Radio ORANGE 94.0'
  export var streamDefaultCover: string =
    'https://aura.radio/assets/aura-logo-grayscale.png'
  export var urlEpisodeDetail: string = '/episode-detail.html?id='
  export var urlShowDetail: string = '/show-detail.html?slug='
  export var urlShowList: string = '/show-list.html'

  setContext('player', {
    renderHtml: renderHtml,
  })

  const TABS: Array<{ [id: string]: string }> = [
    { id: 'now', icon: 'aura-icon-small icon-now', label: 'Now' },
    {
      id: 'programme',
      icon: 'aura-icon-small icon-programme',
      label: 'Programme',
    },
    {
      id: 'playlist',
      icon: 'aura-icon-small icon-playlist',
      label: '',
    },
  ]

  let activeTab: { [id: string]: string } = TABS[0]
  let buttonPlayPause: any = null

  //
  // Custom Tracks
  //

  /* Listen to event for adding/playing a custom track from store */
  let newTrack: any = {}
  const unsubscribeCustomTrack = addTrackStore.subscribe(
    (value: any) => (newTrack = value),
  )
  $: if (newTrack) {
    console.log('Got new track', newTrack)
    trackQueue.push(newTrack)
    trackQueue = trackQueue
    if (newTrack.play) Amplitude.playSongAtIndex(trackQueue.length - 1)
  }

  /* Add any given media URL in the query parameters to the playlist */
  function addCustomTrackFromURL() {
    let url = new URL(window.location.href)
    let urlTrackInfo = url.searchParams.get('track')

    if (urlTrackInfo) {
      let trackString = decodeUTF8(urlTrackInfo)
      let track: any

      try {
        if (trackString)
          track = JSON.parse(trackString)
      } catch (error) {
        console.log(`Error while parsing track in URI: '{trackString}'`, error)
      }
      if (track) {
        // Play immediately or queue only
        let playMediaInUrl = url.searchParams.get('play')
        console.log('Add track via pop-up URL params:', track)
        trackQueue.push(track)
        trackQueue = trackQueue
        if (playMediaInUrl) Amplitude.playSongAtIndex(trackQueue.length - 1)
      }
    }
  }

  //
  // Live Stream
  //

  /* The live stream playlist item (default) */
  let playlistItemLive = {
      name: streamName,
      artist: 'LIVE',
      album: '',
      url: streamUrl,
      live: true,
      cover_art_url: streamDefaultCover,
    }
  /* The currently playing item (default: live stream) */
  let playlistItemCurrent = playlistItemLive
  /* Queue holding the playlist entries */
  let trackQueue = [
      playlistItemLive
    ]

  /* Subscrible to store event on current LIVE episode updates */
  const unsubscribeCurrentEpisode = currentEpisodeStore.subscribe((value: any) =>
    updateLiveMetadata(value),
  )
  function updateLiveMetadata(episode: any) {
    if (episode?.show) {
      playlistItemLive.artist = "LIVE: " + episode.show.name
      if (episode.note_title) {
        // FIXME replace "note_title" with final attribute name
        playlistItemLive.name = episode.note_title
      } else {
        playlistItemLive.name = "-"
      }
      if (episode.show.image)
        playlistItemLive.cover_art_url = episode.show.image
      else
        playlistItemLive.cover_art_url = streamDefaultCover

      // Re-render playlist item
      playlistItemLive = playlistItemLive
      updateCurrentlyPlaying()
    }
  }

  //
  // General
  //

  /* Update reference to the currently playing item */
  function updateCurrentlyPlaying() {
    let index = Amplitude.getActiveIndex()
    let song = Amplitude.getSongAtIndex(index)

    if (song?.live) {
      playlistItemCurrent = playlistItemLive
    } else {
      let index = Amplitude.getActiveIndex()
      let song = Amplitude.getSongAtIndex(index)
      playlistItemCurrent = song
    }
  }

  /* Initialize the component */
  onMount(() => {
    console.log('Initialize AURA Media Player')
    Amplitude.init({
      songs: trackQueue,
      volume: 100,
      volume_increment: 10,
      volume_decrement: 10,
      bindings: {
        80: 'play_pause',
        38: 'volume_increment',
        40: 'volume_decrement',
        78: 'next',
      },
      debug: debug,
      callbacks: {
        initialized: () => {
          if (autoPlay) Amplitude.play()
          updateCurrentlyPlaying()
        },
        pause: () => {
          console.log('Clicked <pause>')
        },
        play: () => {
          buttonPlayPause.classList.add('amplitude-playing')
          buttonPlayPause.classList.remove('amplitude-paused')
          buttonPlayPause.classList.remove('animation-pulse')
          console.log('Clicked <play>')
        },
        playing: () => {
          console.log('Playing...')
        },
        song_change: () => {
          console.log('Song changed')
          updateCurrentlyPlaying()
        },
        stop: () => {
          console.log('Stopped playing')
        },
        error: (e: any) => {
          buttonPlayPause.classList.add('animation-pulse')
          console.log('Error in media-player', e)
        },
      },
    })

    addCustomTrackFromURL()
  })

  function isPlaying(): boolean {
    return Amplitude.getActiveIndex() >= 0
  }

  function playSong(index: number) {
    if (isPlaying()) Amplitude.playSongAtIndex(index)
    else Amplitude.pause
  }

  /* Destroy the component and events */
  onDestroy(unsubscribeCustomTrack)
  onDestroy(unsubscribeCurrentEpisode)
</script>

<style lang="scss">
  /* Media Player */
  #aura-media-player {
    width: 344px;
    background-color: #ffffff;
    box-shadow: 0px 12px 24px rgba(0, 0, 0, 0.12);
    border-radius: 8px;
    margin-left: auto;
    margin-right: auto;
    position: relative;
  }
  #aura-media-player #aura-media-player-display {
    width: 344px;
    height: 344px;
    overflow: hidden;
  }

  /* Media Player Controls */

  #aura-media-player-controls {
    text-align: center;
    padding-bottom: 35px;
  }
  #aura-media-player-controls #play-pause {
    width: 85px;
    height: 85px;
    margin-right: 16px;
    cursor: pointer;
    display: inline-block;
    vertical-align: middle;
    border-radius: 50%;
    background-color: var(--mdc-theme-primary);
  }
  #aura-media-player-controls #play-pause .icon-pp {
    width: 100%;
    height: 100%;
    display: block;
    background-repeat: no-repeat;
    background-size: 100px 100px;
    background-position: center;
    background-color: var(--mdc-theme-background);
    opacity: 0.8;
    -webkit-mask-size: 99px;
    mask-size: 99px;
    -webkit-mask-position: center;
    mask-position: center;
  }
  #aura-media-player-controls
    div#play-pause.amplitude-paused
    .icon-play-circle {
    display: block;
  }
  #aura-media-player-controls
    div#play-pause.amplitude-playing
    .icon-play-circle {
    display: none;
  }
  #aura-media-player-controls
    div#play-pause.amplitude-paused
    .icon-pause-circle {
    display: none;
  }
  #aura-media-player-controls
    div#play-pause.amplitude-playing
    .icon-pause-circle {
    display: block;
  }

  /* Tab: Now */

  #aura-media-player #aura-media-player-tab-now .aura-player-album-art {
    box-shadow: 0px 12px 24px rgba(0, 0, 0, 0.12);
    display: block;
    width: 344px;
    height: 344px;
    background-size: 344px 344px;
    background-position: center;
    background-size: cover;
  }

  /* Tab: Playlist */

  #aura-media-player-playlist-container {
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    bottom: 0;
    background-color: white;
    z-index: 9999;
    display: none;
    border-radius: 8px;
  }
  div.aura-media-player-playlist {
    margin-top: 32px;
    height: 325px;
    overflow-y: scroll;
  }
  div.aura-media-player-playlist-song {
    border-bottom: 1px solid #f5f5f6;
    padding-top: 8px;
    padding-bottom: 8px;
    cursor: pointer;
  }
  div.aura-media-player-playlist-song:hover,
  :global(div.aura-media-player-playlist-song.amplitude-playing) {
    background-color: var(--mdc-theme-surface);
  }
  div.aura-media-player-playlist-song img {
    width: 48px;
    height: 48px;
    border-radius: 3px;
    margin-left: 16px;
    float: left;
  }
  div.aura-media-player-playlist-song div.playlist-song-meta {
    float: left;
    margin-left: 15px;
    width: calc(100% - 80px);
  }
  div.aura-media-player-playlist-song
    div.playlist-song-meta
    span.playlist-song-name {
    color: #414344;
    font-size: 14px;
    display: block;
    width: 100%;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
  div.aura-media-player-playlist-song
    div.playlist-song-meta
    span.playlist-artist-album {
    color: #aaafb3;
    font-size: 12px;
    display: block;
    width: 100%;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
  div.aura-media-player-playlist-song::after {
    content: '';
    display: table;
    clear: both;
  }

  /* SMUI components need to be selected via global namespace */

  :global(#aura-media-player #aura-media-player-tabs .mdc-tab-bar) {
    margin-bottom: 4px;
  }
  :global(#aura-media-player .tab-icon) {
    background-color: var(--mdc-theme-text-primary-on-background);
  }
  :global(#aura-media-player .aura-programme-mini .smui-paper) {
    background-color: var(--mdc-theme-surface);
    padding: 0;
    height: 344px;
    overflow-y: auto;
    scroll-behavior: smooth;
  }
  :global(#aura-media-player .aura-programme-mini .smui-paper td) {
    padding: 12px;
  }
  :global(#aura-media-player .aura-programme-mini .smui-paper td.episode-time) {
    background-color: var(--mdc-theme-text-hint-on-dark);
  }
</style>

<template>
  <div class="aura-media-player-container">
    <div id="aura-media-player">
      <div id="aura-media-player-tabs">
        <TabBar tabs={TABS} let:tab minWidth bind:active={activeTab}>
          <Tab {tab}>
            {#if tab.icon}
              <Icon class="tab-icon {tab.icon}" />
            {/if}
            <!-- <Label>{tab.label}</Label> -->
          </Tab>
        </TabBar>
      </div>

      <div id="aura-media-player-display">
        {#if activeTab && activeTab.id === 'now'}
          <div id="aura-media-player-tab-now" class="tab-content">
            <div
              class="aura-player-album-art"
              style="background-image:url({playlistItemCurrent.cover_art_url})" />
          </div>
        {:else if activeTab && activeTab.id === 'programme'}
          <div
            id="aura-media-player-tab-programme"
            class="tab-content aura-programme-mini">
            <Programme
              view="paper"
              limitCount={5}
              startActive={true}
              renderLinks={renderProgrammeLinks}
              {urlEpisodeDetail}
              {urlShowDetail}
              {urlShowList} />
          </div>
        {:else if activeTab && activeTab.id === 'playlist'}
          <div id="aura-media-player-tab-playlist" class="tab-content">
            <div id="aura-media-player-playlist-container" />
            <div class="aura-media-player-playlist">

              <!-- Populate track queue -->
              {#if trackQueue}
                <div class="card-container list">
                  {#each trackQueue as track, index}
                    <div
                      on:click={() => playSong(index)}
                      class="aura-media-player-playlist-song
                      amplitude-song-container amplitude-play-pause"
                      data-amplitude-song-index={index}>
                      <img alt="" src={track.cover_art_url} />

                      <div class="playlist-song-meta">
                        <span class="playlist-song-name">{track.name}</span>
                        <span class="playlist-artist-album">
                          {track.artist}
                          {@html track.album ? '&bull;' : ''}
                          {track.album}
                        </span>
                      </div>
                    </div>
                  {/each}
                </div>
              {:else}
                <Spinner />
              {/if}

            </div>
          </div>
        {:else}
          <h1 class="error">Error: invalid tab state</h1>
        {/if}
      </div>

      <MediaPlayerCenter {playlistItemCurrent} />

      <div id="aura-media-player-controls">
        <div
          id="play-pause"
          class="aura-media-player-play-pause amplitude-play-pause
          animation-pulse"
          bind:this={buttonPlayPause}>
          <icon class="icon-pp icon-play-circle" />
          <icon class="icon-pp icon-pause-circle" />
        </div>
      </div>
    </div>
  </div>

</template>
