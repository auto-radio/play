import { writable } from 'svelte/store'

export const customUrl = writable('') // e.g. https://cba.fro.at/544097

export const addTrackStore = writable()
