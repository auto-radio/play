import { writable } from 'svelte/store';
import { fetchApi, settings } from './common/Common.svelte';

// Store for current episode
export let currentEpisode = null
export const currentEpisodeStore = writable()

let url = `${settings.api.url}/${settings.api.endpoints.programmeCurrent}`
let pollRefresh = settings.api.pollRefresh

export function poll(url, callback, refreshTime) {
		fetchApi(url, callback)
		const interval = setInterval(() => {
		fetchApi(url, callback)
		}, refreshTime * 1000)
		return () => {
		clearInterval(interval)
		}
  }

function callbackCurrentTimeslot(data) {
	if (data?.timeslot != currentEpisode?.timeslot) {
		currentEpisode = data
		console.log('[currentEpisodeStore] Update current episode from API:', data)

		// FIXME Temporary workaround, since the API response doesn't hold show details
		// @see https://gitlab.servus.at/aura/steering/-/issues/97
		if (data.show) {
			let endpointShow = settings.api.endpoints.show
			let showUrl = `${settings.api.url}/${endpointShow}/${data.show}`
			console.log('Show API Url:' + showUrl)
			fetchApi(showUrl, callbackCurrentShow)
		}
	} else {
		// console.log("Debug: Got data from API, but it didn't change:", data)
	}
}

function callbackCurrentShow(data) {
	currentEpisode.show = data
	currentEpisodeStore.update(() => currentEpisode)
}

poll(url, callbackCurrentTimeslot, pollRefresh);
