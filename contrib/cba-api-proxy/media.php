<?php
// # CBA API Proxy
// #
// # Copyright (C) 2023 - now() - The Aura Team
// #
// # This program is free software: you can redistribute it and/or modify
// # it under the terms of the GNU Affero General Public License as published by
// # the Free Software Foundation, either version 3 of the License, or
// # (at your option) any later version.
// #
// # This program is distributed in the hope that it will be useful,
// # but WITHOUT ANY WARRANTY; without even the implied warranty of
// # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// # GNU Affero General Public License for more details.
// #
// # You should have received a copy of the GNU Affero General Public License
// # along with this program.  If not, see <http://www.gnu.org/licenses/>.

$origin = '';
if (isset($_SERVER['HTTP_ORIGIN']) && $_SERVER['HTTP_ORIGIN'] !== '') {
    $origin = $_SERVER['HTTP_ORIGIN'];
} elseif (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] !== '') {
    $origin = $_SERVER['HTTP_REFERER'];
}

$allowed_origins = [
    'https://play.aura.radio',
    'http://localhost:5000',
    'http://127.0.0.1:5000',
    'http://0.0.0.0:5000',
    'http://o94.home',
    'https://www-dev.o94.at',
    'https://o94.at',
];

if (in_array($origin, $allowed_origins)) {
    header("Access-Control-Allow-Origin: $origin");
} else {
    header('Access-Control-Allow-Origin: https://play.aura.radio');
}

header('Content-Type: application/json; charset=UTF-8');
header('Access-Control-Allow-Methods: GET');
header('Access-Control-Max-Age: 3600');
header(
    'Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With'
);

$apiUrl = 'https://cba.fro.at/wp-json/wp/v2/media';
$apiQuery = $_SERVER['QUERY_STRING'];
$apiRequest = $apiUrl . '?' . $apiQuery;

$c = curl_init();
curl_setopt($c, CURLOPT_URL, $apiRequest);
curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
$response = curl_exec($c);
curl_close($c);

echo $response;
?>
