# AURA CBA Proxy

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](code_of_conduct.md) [![Latest Release](https://gitlab.servus.at/aura/play/-/badges/release.svg)](https://gitlab.servus.at/aura/play/-/releases) [![pipeline status](https://gitlab.servus.at/aura/play/badges/main/pipeline.svg)](https://gitlab.servus.at/aura/play/-/commits/main)

The CBA Proxy is a small PHP proxy, forwarding API requests to CBA.

It allows you to use the CBA API locally during development, circumventing any CORS restrictions.

This proxy is not meant for production.

## Available API endpoints

- `/media`: Pass the URL parameters `api_key` and `parent` with the value of the media ID. It will return JSON with media metadata.