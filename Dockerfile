FROM node:16.17-buster as base
LABEL maintainer="David Trattnig <david.trattnig@subsquare.at>"

# Base
WORKDIR /srv
COPY . .
RUN mkdir -p ./public/build
RUN npm i -g npm@latest
RUN cp config/dev-settings.json config/settings.json
RUN npm run build

# Development
FROM base AS dev
EXPOSE 5000
CMD ["npm", "run", "dev"]

# Production
FROM nginx:1.19-alpine AS prod
COPY --from=base /srv/public /usr/share/nginx/html

